let tabs = document.querySelectorAll('[data-target]');
let tabsContent = document.querySelectorAll('[data-tab-content]');

tabs.forEach(tab =>{
    tab.addEventListener('click',() => {
        tabs.forEach(tab =>{
            tab.classList.remove('active');
        });
        const target = document.getElementById(tab.dataset.target);
        target.classList.add('active');
        tabsContent.forEach(tabContent => {
            tabContent.classList.remove('active');

        });
        const targetContent = document.getElementById(tab.dataset.target.slice(1,tab.dataset.target.length));
        targetContent.classList.add('active');
    })
});
